//
//  FamousPeopleCell.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "FamousPeopleCell.h"

#define PICTURE_FIELD @"picture"
#define SPEECH_DATE_FIELD @"speechDate"
#define NAME_FIELD @"name"

@implementation FamousPeopleCell


- (void)configureWithPath:(NSString*)dataPath
{
    _dataPath = dataPath;
    
    __weak FamousPeopleCell *weak_self = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSDictionary *personData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
        
        if (![weak_self.dataPath isEqualToString:dataPath])
            return;
        
        UIImage *image = [UIImage imageNamed:personData[PICTURE_FIELD]];
        
        if (![weak_self.dataPath isEqualToString:dataPath])
            return;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d.MM.YYYY"];
        NSString *date = [formatter stringFromDate:personData[SPEECH_DATE_FIELD]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (![weak_self.dataPath isEqualToString:dataPath])
                return;
            
            [weak_self.avatarImageView setImage:image];
            weak_self.nameLabel.text = personData[NAME_FIELD];
            weak_self.dateLabel.text = date;
        });
    });
}

@end
