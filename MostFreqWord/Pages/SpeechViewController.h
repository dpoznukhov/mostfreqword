//
//  SpeechViewControllerTableViewController.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeechViewController : UIViewController

- (void)configureWithDataPath:(NSString*)data;


@end
