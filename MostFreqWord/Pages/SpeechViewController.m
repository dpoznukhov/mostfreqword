//
//  SpeechViewControllerTableViewController.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "SpeechViewController.h"
#import "FrequentCalculator.h"

#define SPEECH_FIELD @"speech"

@interface SpeechViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (nonatomic, strong)VoidBlock cancelCalcBlock;
@property (nonatomic)BOOL isDisappeared;

@end

@implementation SpeechViewController
{
    NSString *dataPath;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _isDisappeared = NO;
    [self loadData];
}

- (void)loadData
{
    __weak SpeechViewController *weak_self = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if (weak_self.isDisappeared)
            return;
        
        NSDictionary *personData = [NSDictionary dictionaryWithContentsOfFile:dataPath];
        
        if (weak_self.isDisappeared)
            return;
        
        NSString *speech = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:personData[SPEECH_FIELD] ofType:@"txt"]
                                                     encoding:NSUTF8StringEncoding
                                                        error:nil];
        
        if (weak_self.isDisappeared)
            return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weak_self.textView.text = speech;
        });
        
        if (weak_self.isDisappeared)
            return;
        
        weak_self.cancelCalcBlock =
        [[FrequentCalculator instance] findMostFrequentWordInStringReturnCancelBlock:speech
                                                                       progressBlock:^(float value) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               [weak_self.progressView setProgress:value];
                                                                           });
                                                                       } completionBlock:^(NSString *string) {
                                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                                               weak_self.resultLabel.text = string;
                                                                               weak_self.resultLabel.hidden = NO;
                                                                               weak_self.progressView.hidden = YES;
                                                                           });
                                                                       }];
    });
}

- (void)configureWithDataPath:(NSString*)path
{
    dataPath = path;
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (_cancelCalcBlock)
        _cancelCalcBlock();
    
    _isDisappeared = YES;
}

- (void)dealloc
{
    int g = 0;
}

@end
