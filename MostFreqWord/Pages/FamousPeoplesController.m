//
//  FamousPeoplesController.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "FamousPeoplesController.h"
#import "FamousPeopleCell.h"
#import "SpeechViewController.h"

#import "SeguesIDs.h"

#define LIST_NAME @"Persons"
#define REUSE_ID @"PersonCell"

#define PERSONS_FIELD @"persons"

@interface FamousPeoplesController()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FamousPeoplesController
{
    NSDictionary *persons;
}

- (void)viewDidLoad
{
    NSString *listPath = [[NSBundle mainBundle] pathForResource:LIST_NAME ofType:@"plist"];
    
    persons = [NSDictionary dictionaryWithContentsOfFile:listPath];
    
}

#pragma mark - TableView Data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [persons[PERSONS_FIELD] count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FamousPeopleCell *cell = (FamousPeopleCell*)[tableView dequeueReusableCellWithIdentifier:REUSE_ID];
    
    [cell configureWithPath:[[NSBundle mainBundle] pathForResource:[persons[PERSONS_FIELD]
                                                                    objectAtIndex:indexPath.row] ofType:@"plist"]];
    
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:FromListToSpitchViewSegue]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        [segue.destinationViewController configureWithDataPath:[[NSBundle mainBundle] pathForResource:[persons[PERSONS_FIELD]
                                                                                                       objectAtIndex:indexPath.row] ofType:@"plist"]];
    }
}


@end
