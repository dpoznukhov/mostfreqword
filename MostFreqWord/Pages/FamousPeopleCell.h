//
//  FamousPeopleCell.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FamousPeopleCell : UITableViewCell

- (void)configureWithPath:(NSString*)dataPath;

@property (nonatomic, strong)NSString *dataPath;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
