//
//  FamousPeoplesController.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FamousPeoplesController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
