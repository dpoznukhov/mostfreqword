//
//  FrequentCalculator.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "FrequentCalculator.h"
#import "CountedHashTable.h"

#define CORES_COUNT 1

@implementation FrequentCalculator
{
    NSOperationQueue *internalQueue;
}

+ (instancetype)instance
{
    static FrequentCalculator *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[FrequentCalculator alloc] init];
        instance->internalQueue = [[NSOperationQueue alloc] init];
        instance->internalQueue.name = @"FrequentCalculator";
        instance->internalQueue.maxConcurrentOperationCount = CORES_COUNT;
    });
    return instance;
}

- (VoidBlock)findMostFrequentWordInStringReturnCancelBlock:(NSString*)string
                                             progressBlock:(FloatBlock)progressBlock
                                           completionBlock:(StringBlock)completionBlock
{
    NSBlockOperation *operaion = [[NSBlockOperation alloc] init];
    
    __weak NSBlockOperation *weak_operation = operaion;
    
    [operaion addExecutionBlock:^{
        NSArray *words = [string componentsSeparatedByString:@" "];
        
        if (weak_operation.isCancelled)
            return;
        
        CountedHashTable *table = [[CountedHashTable alloc] init];
        
        if (weak_operation.isCancelled)
            return;
        
        NSInteger i = 0;
        for (NSString *word in words){
            
            if (weak_operation.isCancelled)
                return;
            
            [table addKey:word];
            
            progressBlock((float)(i++) / (float)words.count);
        }
        completionBlock([table mostFrequentKey]);
    }];
    
    [internalQueue addOperation:operaion];

    return ^{
        [weak_operation cancel];
    };
}

@end
