//
//  FrequentCalculator.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomBlocks.h"

@interface FrequentCalculator : NSObject

+ (instancetype)instance;

- (VoidBlock)findMostFrequentWordInStringReturnCancelBlock:(NSString*)string
                                             progressBlock:(FloatBlock)progressBlock
                                           completionBlock:(StringBlock)completionBlock;

@end
