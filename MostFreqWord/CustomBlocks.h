//
//  CustomBlocks.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef void (^VoidBlock)(void);
typedef void (^FloatBlock)(float value);
typedef void (^StringBlock)(NSString *string);
