//
//  HashTable.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountedHashTable : NSObject

- (void)addKey:(NSString*)key;

- (NSString*)mostFrequentKey;

- (void)clear;

@end
