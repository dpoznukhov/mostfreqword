//
//  LinkedList.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "LinkedList.h"
#import "LinkedList_Private.h"


@implementation LinkedList
{
    NSInteger count;
}


- (instancetype)initWithObject:(id)data
{
    self = [super init];
    if (self){
        self.data = data;
        count = 1;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self){
        count = 0;
    }
    return self;
}

- (void)addObject:(id)data
{
    if (!_data){
        _data = data;
        count = 1;
        return;
    }
    
    if (_next){
        [_next addObject:data];
        return;
    }
    
    _next = [[LinkedList alloc] initWithObject:data];
    ++count;
}

- (NSInteger)count
{
    return count;
}






- (void)setNext:(LinkedList *)next
{
    _next = next;
    _next->_prev = self;
}

- (void)setPrev:(LinkedList *)prev
{
    _prev = prev;
    _prev->_next = self;
}

@end
