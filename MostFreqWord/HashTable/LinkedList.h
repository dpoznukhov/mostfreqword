//
//  LinkedList.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinkedList : NSObject

@property (nonatomic, strong)id data;


- (instancetype)initWithObject:(id)data;
- (void)addObject:(id)data;
- (NSInteger)count;



@end
