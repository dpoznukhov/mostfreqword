//
//  HashTable.m
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

#import "CountedHashTable.h"
#import "LinkedList.h"
#import "LinkedList_Private.h"

@interface BucketNode : NSObject

@property (nonatomic, strong)NSString *key;
@property (nonatomic)NSInteger count;

@end

@implementation BucketNode

+ (instancetype)nodeWithKey:(NSString*)key
{
    BucketNode *node = [[BucketNode alloc] init];
    node.key = key;
    node.count = 1;
    
    return node;
}

@end

unsigned int HashFAQ6(const char * str){
    
    unsigned int hash = 0;
    
    for (; *str; str++){
        hash += (unsigned char)(*str);
        hash += (hash << 10);
        hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    
    return hash;
}

#define TABLE_BUCKETS_COUNT 1024
#define HASH_MASK 0b1111111111

@implementation CountedHashTable
{
    NSMutableArray *buckets;
}

- (instancetype)init
{
    self = [super init];
    if (self){
        buckets = [NSMutableArray arrayWithCapacity:TABLE_BUCKETS_COUNT];
        [self clear];
    }
    return self;
}

- (void)addKey:(NSString*)key
{
    unsigned int hash = HashFAQ6(key.UTF8String);
    hash &= HASH_MASK;
    
    LinkedList *bucket = buckets[hash];
    
    for (LinkedList *listNode = bucket; listNode; listNode = listNode.next){
        BucketNode *bucketNode = listNode.data;
        if ([bucketNode.key isEqualToString:key]){
            bucketNode.count++;
            return;
        }
    }
    
    [bucket addObject:[BucketNode nodeWithKey:key]];
}

- (NSString*)mostFrequentKey
{
    NSInteger freqCount = 0;
    NSString *freqKey = nil;
    
    
    for (LinkedList *bucket in buckets){
        for (LinkedList *listNode = bucket; listNode; listNode = listNode.next){
            BucketNode *bucketNode = listNode.data;
            if (bucketNode.count > freqCount){
                freqCount = bucketNode.count;
                freqKey = bucketNode.key;
            }
        }
    }
    return freqKey;
}

- (void)clear
{
    [buckets removeAllObjects];
    for (NSInteger i = 0; i < TABLE_BUCKETS_COUNT; ++i){
        [buckets addObject:[[LinkedList alloc] init]];
    }
}

@end
