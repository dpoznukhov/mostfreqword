//
//  LinkedList_Private.h
//  MostFreqWord
//
//  Created by Dmitriy Poznuhov on 22/10/15.
//  Copyright © 2015 Dmitriy Poznuhov. All rights reserved.
//

@interface LinkedList()

@property (nonatomic, strong)LinkedList *next;
@property (nonatomic, weak)LinkedList *prev;


@property (nonatomic, weak)LinkedList *lastObject;

@end
